// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var md5 = require('md5');
var cookie = require('cookie');
var jwt = require("jwt-simple");
app.set('jwtTokenSecret','test');
// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });
// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});
app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});
app.post("/login", function(req, res, next) {
	db.get('SELECT rowid, ident, password FROM users where ident=? and password=?',[req.body.login,md5(req.body.mdp)], function(err,data)
	{
	if(data)
	{
		var token = jwt.encode({
			ident: req.body.login , 
			password: req.body.password
		}, app.get('jwtTokenSecret'));
		//res.send('Vous étes connectés');
		res.cookie('token',token);
		res.json({status:true});
		db.get('SELECT * FROM sessions where ident = ?',[req.body.login], function(err, data) {
		if(data){ db.run('UPDATE sessions set token=?',[token]);}
		else{ db.run('INSERT INTO sessions (ident,token) values (?,?)',[req.body.login,token]);}
	});
	}
	else{ 
	//res.send("Erreur de connection");
	res.json({status:false}); }
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
